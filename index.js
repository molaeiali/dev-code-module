const axios = require('axios').default;

exports.send = function (token, username, code, selfHostedDomain = "http://dev-code.ir") {
    return axios.get(selfHostedDomain + "/send?token=" + token + "&uname=" + encodeURIComponent(username) + "&text=" + encodeURIComponent(code));
}
