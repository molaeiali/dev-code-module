[![npm version](https://badge.fury.io/js/dev-code.svg)](https://badge.fury.io/js/dev-code)

# README

A helper for using [Dev Code Telegram Bot](https://gitlab.com/molaeiali/dev-code-bot) in node projects

## Installation
```
npm install dev-code
```
or
```
yarn add dev-code
```
## Usage

```js
const devcode = require("dev-code");

devcode.send(
    "DEVCODE_TOKEN",
    "USERNAME",
    "CODE",
    "SELF_HOSTED_DOMAIN"
).then((response)=>{
    // Do your work
}).catch((error)=>{
    // Handle network error
});
```
`SELF_HOSTED_DOMAIN` is optional, use it if you self-hosted the telegram bot

By default, `http://dev-code.ir` will be used

# Attributions
Icon made by Freepik from www.flaticon.com
