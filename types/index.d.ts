import { AxiosPromise } from "axios";

export function send(
  token: string,
  username: string,
  code: string,
  selfHostedDomain?: string
): AxiosPromise;
